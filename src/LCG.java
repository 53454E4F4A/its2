
import java.util.HashMap;
import java.util.Random;
import java.util.Vector;

public class LCG {
	
	static final long parameters[][] = {
		{	9496, 0, 32749 },  				  	// Chiang, Hwang, Kao
			{	214013,	 13523655, 32749 }, // 	// BASIC, N = 2^15-19
			{	16598013, 12820163, 16777216},		// Qbasic N = 2^24
			{	62605, 113218009, 16777216},		// Berkeley UNIX Pascal
			{	452807053, 0, 536870912 },			// URN12 N = 2^29
			{	16807, 0, 2147483647 },				// _Minimal Standard_ von Lewis, et al. N = 2^31-1
			{	41358, 0, 2147483647 }, 			// L'Ecuyer
			{	48271, 0, 2147483647 }, 			// Park & Miller
			{	51081, 0, 2147483647 }, 			// H�rtel
			{	69621, 0, 2147483647 }, 			//Park & Miller
			{	950706376, 0, 2147483647}, 			//FISH von Fishman & Moore
			{	63060016, 0, 2147483647}, 			// SIMSCRIPT II
			{	397204094, 0, 2147483647}, 			// SAS & IMSL-Library von Hoaglin
			{	65539, 0, 2147483648L}, 				// IBM's RANDU, N = 2^31
			{	1103515245, 12345, 2147483648L}, 		// UNIX rand [Rip90], ANSI C
			{	129, 907633385, 4294967296L}, 			// Turbo Pascal
			{	69069, 1, 4294967296L}, 				// VAX VMS-Generator von Marsaglia
			{	663608941, 0, 4294967296L},		    // C-RAND von Ahrens
			{	1099087573, 0, 4294967296L}, 			// Fishman
			{	3141592653L, 1, 4294967296L}, 			// DERIVE
			{	2147001325, 715136305, 4294967296L}, 	// BCPL von Richards, Whitby-Strevens
			{	30517578125L, 7261067085L, 34359738368L}, 		// Boeing Computer Services BCSLIB N = 2^35
			{	71971110957370L, 0, 140737488355213L}, 	// L'Ecuyer N = 2^47-115
			{	25214903917L, 11, 281474976710656L}, 		// SUN-UNIX drand48 2^48
			{	44485709377909L, 0, 281474976710656L}, 		// Cray X-MP Library
			{	68909602460261L, 0, 281474976710656L}, 		// Fishman
			{	302875106592253L, 0, 576460752303423488L}, 				// NAG Fortran Library N = 2^59
			{	2307085864L, 0, 9223372036854775783L}, 		// L'Ecuyer N= 2^63-25
			{	427419669081L, 0, 999999999989L}, 	// MAPLE N = 10^12-11;
			{ 	25214903917L, 11, 281474976710656L} }; // JAVA
	
	public static final String keys[] = {
		"Chiang, Hwang, Kao",
		"BASIC",
		"Qbasic",
		"Berkeley UNIX Pascal",
		"URN12",
		"Minimal Standard",
		"L'Ecuyer",
		"Park & Miller",
		"H�rtel",
		"Park & Miller 2",
		"FISH",
		"SIMSCRIPT II",
		"SAS",
		"IMB's RANDU",
		"UNIX rand",
		"Turbo Pascal",
		"VAX VMS-Generator",
		"C-RAND",
		"Fishman",
		"DERIVE",
		"BCPL",
		"Boeing Computer Services",
		"L'Ecuyer 2", 
		"SIM-UNIX drand48",
		"Cray X-MP Library",
		"Fishman 2",
		"NAG Fortran Library",
		"L'Ecuyer 3",
		"MAPLE",
		"Java" };

	public final HashMap<String, long[]> params = new HashMap<String, long[]>();
	long a = 0;
	long b = 0;
	long n = 0;

	long z = 0;
	public LCG(long seed) {
		for(int i = 0; i < keys.length; i++) {
			params.put(keys[i], parameters[i]);
		}
		this.z = seed;
		setConfiguration("Java");
	}
	
	public LCG(long a, long b, long n, long seed) {
		for(int i = 0; i < keys.length; i++) {
			params.put(keys[i], parameters[i]);
		}
		
		
		this.a = a;
		this.b = b;
	
		this.n = n;
		this.z = seed;
	}
	public void setConfiguration(String key) {
		this.a = params.get(key)[0];
		this.b = params.get(key)[1];
		this.n = params.get(key)[2];
	}

	public byte nextByte() {
		
		
		return  (byte)(nextValue());
		
	}
	public long nextValue(){
		
		z = ( z * a + b) %n;
		
		return z;
	}

	
	public void setParameters(long a, long b, long n,  long z) {
		if(a != 0) this.a = a;
		if(b != 0) this.b = b;

		if(n != 0) this.n = n;
		if(z != 0) this.z = z;
	}
	

	

}