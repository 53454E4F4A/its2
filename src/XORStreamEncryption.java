import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class XORStreamEncryption {
	
static void xor_lcg(String inpath, String outpath, long seed, String profile) {
	LCG lcg ;
	lcg = new LCG(seed);
	lcg.setConfiguration(profile);
	try (FileInputStream  in = new FileInputStream(inpath);
		FileOutputStream out = new FileOutputStream(outpath)) {
		
		int b;
		while((b = in.read()) != -1) 
			out.write(b ^ lcg.nextByte());
		
		in.close();
		out.close();
	  
	} catch (IOException x) {
	    System.err.println(x);
	}
}

public static void main(String[] args) {
	try {
		if(args[0].equals("list")) {
			for(String s : LCG.keys) System.out.println(s);
		} else if(args[0].equals("show")) {
			LCG lcg = new LCG(1);	
			long[] params = lcg.params.get(args[1]);
			System.out.println("a = " + params[0] + "\nb = " + params[1] + "\nm = " + params[2]);
		} else {
			String profile = args[3];
			if(args.length != 4) profile = "Java";
			XORStreamEncryption.xor_lcg(args[0], args[1], Long.parseLong(args[2]), profile);
		}
	} catch (Exception e) {
		System.out.println("usage: java XORStreamEncryption <in file> <out file> <long seed> [profile]");
		System.out.println("list profiles: java XORStreamEncryption list");
		System.out.println("show profile: java XORStreamEncryption show <profile>");
	}
}


}

