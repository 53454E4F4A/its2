import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class TripleDES {

	DES des1;
	DES des2;
	DES des3;
	byte iv[] = new byte[8];
	
	public TripleDES(String keyfile) {
		super();
	
		try (FileInputStream  in = new FileInputStream(keyfile)	) {
				
				
				byte buffer[] = new byte[8];
				in.read(buffer);
				des1 = new DES(buffer);
				in.read(buffer);
				des2 = new DES(buffer);
				in.read(buffer);
				des3 = new DES(buffer);
				in.read(iv);
				
				
				in.close();
			  
			} catch (IOException x) {
			    System.err.println(x);
			}
		
	}
	void tripledes_encrypt(byte[] source, int i, byte[] dest, int j) {
		byte tmp[] = new byte[8];
		byte tmp2[] = new byte[8];
		des1.encrypt(source, i, tmp, j);
		des2.decrypt(tmp, i, tmp2, j);
		des3.encrypt(tmp2, i, dest, j);
	}
	//not needed for cypher feedback
	void tripledes_decrypt(byte[] source, int i, byte[] dest, int j) {
		byte tmp[] = new byte[8];
		byte tmp2[] = new byte[8];
		des1.decrypt(source, i, tmp, j);
		des2.encrypt(tmp, i, tmp2, j);
		des3.decrypt(tmp2, i, dest, j);
	}
	void encrypt(String inpath, String outpath) {
		byte[] c = new byte[8];
		byte[] buffer = new byte[8];
	
		tripledes_encrypt(iv, 0, c, 0);
		try (FileInputStream  in = new FileInputStream(inpath);
				FileOutputStream out = new FileOutputStream(outpath)) {
				
			
				while ((in.read(buffer)) > 0) {
				
				for(int i = 0; i < 8; i++) {
					c[i] = (byte) (c[i] ^ buffer[i]);
				}
				
				out.write(c);
				tripledes_encrypt(c, 0, c, 0);
				} 
				
				in.close();
				out.close();
			  
			} catch (IOException x) {
			    System.err.println(x);
			}
	}
	
	void decrypt(String inpath, String outpath) {
		byte[] c = new byte[8];
		byte[] buffer = new byte[8];
		byte[] buffero = new byte[8];
		tripledes_encrypt(iv, 0, c, 0);
		try (FileInputStream  in = new FileInputStream(inpath);
				FileOutputStream out = new FileOutputStream(outpath)) {
				
			
				while ((in.read(buffer)) > 0) {
				
				for(int i = 0; i < 8; i++) {
					buffero[i] = (byte) (c[i] ^ buffer[i]);
				}
				
				out.write(buffero);
				tripledes_encrypt(buffer, 0, c, 0);
				} 
				
				in.close();
				out.close();
			  
			} catch (IOException x) {
			    System.err.println(x);
			}
	}
	
	
	public static void main(String[] args) {
		try {
			TripleDES td = new TripleDES(args[1]);
			if(args[3].equals("encrypt")) {
				td.encrypt(args[0], args[2]);
			} else if(args[3].equals("decrypt")) {	
				td.decrypt(args[0], args[2]);
			} else {
				System.out.println("unknown command");
			}
		
		} catch(Exception e) {
			System.out.println("usage: java TripleDES <in file> <key file> <out file> encrypt|decrypt");
		}
	}
	
	
}
